

gcloud artifacts repositories create symfony-docker-repo --repository-format=docker \
--location=europe-west1 --description="Docker repository"


gcloud builds submit --region=europe-west1 --tag europe-west1-docker.pkg.dev/symfony-366809/symfony-docker-repo/symfony-image:tag1
gcloud builds submit --region=europe-west1 --config cloudbuild.yaml


kubectl create secret generic symfony-db-secret \
--from-literal=username=symfony \
--from-literal=password=123456778Th \
--from-literal=database=symfony


gcloud container clusters get-credentials YOURCLUSTERHERE --zone YOURCLUSTERZONEHERE
