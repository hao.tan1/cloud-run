FROM composer:2.3 as build
WORKDIR /app
COPY composer.json composer.lock /app/
RUN composer install --no-dev --no-scripts --no-autoloader \
    && composer dump-autoload --optimize

# Use the official PHP image.
# https://hub.docker.com/_/php
FROM php:8.1-apache
# Configure PHP for Cloud Run.
RUN apt-get update && apt-get install -y \
    acl \
 && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-install pdo
#RUN pecl install xdebug \
#    && docker-php-ext-enable xdebug
# Precompile PHP code with opcache.
RUN docker-php-ext-install -j "$(nproc)" opcache
WORKDIR /var/www/project

ENV APP_ENV=prod
ENV HTTPDUSER='www-data'

COPY docker/000-default.conf /etc/apache2/sites-available/
COPY docker/ports.conf /etc/apache2/
#COPY docker/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

#set default PORT when it is not set
#ENV PORT=${PORT:-80}

# Use the PORT environment variable in Apache configuration files.
# https://cloud.google.com/run/docs/reference/container-contract#port
#RUN sed -i 's/80/${PORT}/g' /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf

COPY docker/php.ini "$PHP_INI_DIR/conf.d/cloud-run.ini"

# Configure PHP for development.
# Switch to the production php.ini for production operations.
# RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
# https://github.com/docker-library/docs/blob/master/php/README.md#configuration
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

# Copy in custom code from the host machine.
COPY --from=build /app/vendor /var/www/project/vendor
COPY . /var/www/project/

RUN mkdir -p /var/www/project/var/log/ && \
    mkdir -p /var/www/project/var/cache/ && \
    usermod -u 1000 www-data &&\
    chown -R www-data:www-data /var/www/  && \
    a2enmod rewrite
USER www-data

RUN php bin/console cache:clear --no-warmup && \
    php bin/console cache:warmup

CMD ["apache2-foreground"]
#EXPOSE 9000