import { ApolloServer } from '@apollo/server';
import { addMocksToSchema } from '@graphql-tools/mock';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { importSchema } from 'graphql-import'
import { startStandaloneServer } from '@apollo/server/standalone';


const typeDefs = importSchema('schema.graphql'); // or .gql or glob pattern like **/*.graphql
const resolvers = {
};
// @ts-ignore
const server = new ApolloServer({
    // addMocksToSchema accepts a schema instance and provides
    // mocked data for each field in the schema
    schema: addMocksToSchema({
        schema: makeExecutableSchema({ typeDefs, resolvers }),
    }),
});

(async () => {
    const { url } = await startStandaloneServer(server, { listen: { port: 4000 } });
    console.log(`🚀 Server listening at: ${url}`);
})();