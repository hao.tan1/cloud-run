# Road map pour améliorer la performance

## État actuel
```mermaid
graph
    A[Website mister-auto] -->B(Mabro)
    B --> C[vue.js - frontend]
    B --> D[node.js - backend]
    C -->|XHR API| E[Mafront]
    E --> J[("Database(traduction)")]
    C -->|XHR API| F[Algolia]
    D -->|Curl API| E[Mafront]
    D -->|Curl API| F
    E --> |Curl| F
    E-->|curl API| G[Maws]
    G -->H[(Database)]
```
## problèmes existant
* Pour la partie front de Mabro: 
```mermaid
graph LR
    C[vue.js- Frontend] -->|XHR API| E[Mafront]
    C -->|XHR API| F[Algolia]
```
    * Deux API différents à gérer (Algolia et Ma-front). 
    * Ma-front retourne des données déjà présentes dans la réponse d'Algolia 
    * l'API de listingProduct est trop lourd

* Pour la partie backend de Mabro
```mermaid
graph LR
    D[Mabro -backend] -->|Curl API| E[Mafront]
    D --> |Curl API| F[Algolia]

```
    * Nuxt-request est trop lourd
    * Trop d'appel entre Mafront et Maws
    * Trop d'appel entre Mafront et Algolia

* Mafront
```mermaid
graph LR
    E[Mafront] --> J[("Database(traduction)")]
    E--> F(Algolia)
    E-->G[maws]
```
    * mafront besoin de récupérer les données en même temps depuis maws et algolia
    * les infos SEO besoin de traduire une deuxième fois dans mafront
## Solution proposée
### Court terme
Améliorer la perforance de page listing: 

* Pour diminuer le ttfb
  * Supprimer les appels doublons entre Mabro et Mafront
  * diminuer la taille de réponse de nuxt-request
* Pour diminuer le temps de charges des prix/dispo
  * ajouter la pagination pour product-listing
  * supprimer les informations inutile dans la réponse de product-listing
* Beyable
  * repositionner l'ordre de chargement XHR

### long terme
* Remplacer Mabro avec API frontend(symfony)
* Restructurer les APIs pour Mabro (plus petit, souple et plus rapid)
* Mabro n'appel que la nouvelle API
* la nouvelle API fournie des endpoint pour Algolia/Elastic-Search
* Diminuer les appels entre la nouvelle API et maws (jusqu'au moment on peut les supprimer)
* ajouter les Cache pour tous les API (avec les stratégies différentes) 

```mermaid
graph
  A[Website mister-auto] --> B{Robot/User?}
    B -->|Robot| C[Prerendering static content]
    B -->|Utilisateur normal| D
    D-->|crawl par| I[Generateur page statique]
    D -->|XHR API|J["Cache(Varnish/Nginx/Symfony)"] --> E[API Backend Symfony]

    subgraph SPA front
       D[SPA Vue.js]
    end
    
    subgraph Nouvelle API 
    E -->|Curl API| F[Algolia]
    E-->|curl API| G[Maws]
    E -->H[(Database maws)]
    E -->K[(Dagabase seo)]
    L[Backoffice SEO] -->K
    end
    subgraph Static server
    I --> |envoyer les pages static|C
    C <--> M[(Google Cloud Storage)]
    end
```
* on supprime les appel maws
* récupère directement les données dans les DB actuelle (maws)

### Idéal pour Backend

```mermaid
graph 
  A(API Backend Symfony) --> |read|B[(DB)]
  A --> |read|C[Elastic Search / Alogolia]
  D["Back office(EasyAdmin/Sonata/Kunstmaan)"] --> B
  D--> |write|C
```

## Advantage avec la nouvelle infrastructure

### Performance
* pour les crawleurs robot, la FFTB peut atteindre 30-80ms, soit 100 fois plus rapide que maintenant
* pour les utilisateurs, le temps de charge du site est inférieur à 800 ms(charge totale) comparer avec le temps qu'on a actuellement : 8000 - 30000 ms

Note: crawler n’est pas utilisateur, cache ne fonctionne pas bien pour robot, mais il marche bien pour les utilisateurs.
Static server est la meilleure solution pour les robots.

### Résilience
* les peaks des robots ne sont plus un problèmes (plus de serveurs node, plus besoin de backend génère les pages)
* plus de problème de serveur node (tout sont côté client)
* Backend est pure Restful API, plus simple a maintenir
* plus besoin d'avoir la dépendance de maws (second temps)
* on peut facilement brancher ElasticSearch/Algolia comme search engine
* une BackOffice unique pour gérer les infos du Front 

### Maintainability du code

Côte API:
* il est beaucoup plus simple comparer avec le code actuel
* Comme il est orienté API, il sera plus rapide
* avec Symofny, c'est plus facile à gérer les cache (niveau applicatif) 

Côté Javascript:
* une refactoring pour adapter la nouvelle API nous impose de faire plus propre le code
* charge les infos le plus tard possible améliorer la performance

### Beayable
* il sear automatiquement réglé si l'outil externe ne nous bloque pas.

##Road map
```mermaid
gantt
dateFormat  YYYY-MM-DD
title Améliorer la performance 
excludes weekdays 2014-01-10

section Mabro
Snacktrail            :done,    des1, 2022-11-01,2022-11-21
Header et footer sans l'appel du nuxt request               :active,afeter des1  des2, 10d
lister composants pages listing(analyse)              :         des3, after des1, 5d
lister composants pages listing(structure de données)            :         des4, after des3, 5d

section MaFront
améliorer/supprimer bloc "meilleures ventes"    :active,  mafront-des1, 2022-11-22, 10d

section Nouvelle API symfony
POC API symfony 6/php8/docker/kub               :active,  symfony-des1, 2022-11-10, 30d
Convertion d'API  : symfony2 , after symfony-des1, 5d
API remplacer nuxt_request         :        symfony3, after symfony2, 40d
API remplacer productListing       :       symfony4, after symfony3, 20d
Autre API SEO/Maillage    :       symfony5, after symfony4, 30d
API rest pour nuxt   :   symfony6, after symfony5, 30d

section nouvelle Mabro (pure SPA vue.js V3) 
Interface avec la nouvelle API symfony  :      mabro-1, after symfony-des1, 10d
Migration/réecrit nuxt2 vers vue3   :   mab*ro-2, after mabro-1, 40d

section générateur cotenu statique
crawler du site     :    gs-1, after symfony-des3, 20d
cron et sauvgard    :   gs-2, after gs-1, 20d
speed test      :   gs-3, after gs-2, 10d
Mise en route les règles de proxy pour les robots   :   gs-4, after devops-5, 5d

section devops
Dockerfile pour nouvelle nouvelle API Symfony       :      devops-1, after symfony-des2, 20d
s'intègre dans le système actuelle  : devops-1-2, after devops-1, 10d
Cache : devops-1-1, after devops-1-2, 10d
s'intègre dans le kube existant     :   devops-2, after devops-1-1, 20d
CI/CD pour le déploiement Mabro SPA     :   devops-3, after devops-2, 10d
création du container pour contenu statique     :   devops-4, after devops-1, 10d
proxy pour séparer les utilisateurs normaux et les robots   :   devops-5,  after devops-4, 10d
```

Estimations : 


    --Nouvelle API Symfony
    un developpeur API -- 2 mois
    -- Refactoring Mabro vers SPA
    un devleoppeur Vue.js -- 2 mois
    -- générateur contenu statique
    un developpeur PHP/Node (dépends d'outil qu'on a choisi)  -- 1 mois
    Devops: (si on garde l'infra actuelle)
    - 20 jours/homme


   
