<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class CatController extends AbstractController
{
    #[Route('/', name: 'app_check')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to symfony kube!',
            'path' => 'src/Controller/CatController.php',
        ]);
    }

    #[Route('/cat', name: 'app_cat')]
    public function cat(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome see cat!',
            'path' => 'src/Controller/CatController.php',
        ]);
    }
}
